<?php
// HTTP
define('HTTP_SERVER', 'http://myasishchev-oskar.ru/admin/');
define('HTTP_CATALOG', 'http://myasishchev-oskar.ru/');

// HTTPS
define('HTTPS_SERVER', 'http://myasishchev-oskar.ru/admin/');
define('HTTPS_CATALOG', 'http://myasishchev-oskar.ru/');

// DIR
define('DIR_APPLICATION', '/home/admin/web/myasishchev-oskar.ru/public_html/admin/');
define('DIR_SYSTEM', '/home/admin/web/myasishchev-oskar.ru/public_html/system/');
define('DIR_IMAGE', '/home/admin/web/myasishchev-oskar.ru/public_html/image/');
define('DIR_STORAGE', '/home/admin/web/myasishchev-oskar.ru/storage/');
define('DIR_CATALOG', '/home/admin/web/myasishchev-oskar.ru/public_html/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'admin_optica');
define('DB_PASSWORD', 'grOsZE1cF1');
define('DB_DATABASE', 'admin_optica');
define('DB_PORT', '3306');
define('DB_PREFIX', '');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
